import { TestBed, inject } from '@angular/core/testing';

import { AutentificacaoService } from './autentificacao.service';

describe('AutentificacaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutentificacaoService]
    });
  });

  it('should be created', inject([AutentificacaoService], (service: AutentificacaoService) => {
    expect(service).toBeTruthy();
  }));
});
