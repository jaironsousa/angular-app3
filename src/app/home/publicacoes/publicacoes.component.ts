import { BdService } from './../../bd.service';
import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase';

@Component({
  selector: 'app-publicacoes',
  templateUrl: './publicacoes.component.html',
  styleUrls: ['./publicacoes.component.css']
})
export class PublicacoesComponent implements OnInit {

  email: string;

  publicacoes: any;

  constructor(
    private bdService: BdService
  ) { }

  ngOnInit() {
    firebase.auth().onAuthStateChanged((user) => {
      this.email = user.email;
      this.atualizarTimeLine();
    });

  }

  public atualizarTimeLine(): void {
    this.bdService.consultarPublicacoes(this.email)
      .then((publicacoes) => {
        this.publicacoes = publicacoes;
        console.log(publicacoes);
      });
  }

}
