import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BdService } from './../../bd.service';
import * as firebase from 'firebase';
import { Progresso } from '../../progresso.service';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-incluir-publicacao',
  templateUrl: './incluir-publicacao.component.html',
  styleUrls: ['./incluir-publicacao.component.css']
})
export class IncluirPublicacaoComponent implements OnInit {

  @Output() public atualizarTimeLine: EventEmitter<any> = new EventEmitter<any>();

  public email: string;

  private imagem: any;

  public progressoPublicacao = 'pendente';

  public porcentagemUpload: number;

  public progress: any;

  public formulario: FormGroup = new FormGroup({
    'titulo': new FormControl(null),
  });


  constructor(
    private bd: BdService,
    private progresso: Progresso
  ) { }

  ngOnInit() {
    firebase.auth().onAuthStateChanged((user) => {
      this.email = user.email;
    });
  }

  public publicar() {
    this.bd.publicar({
      email: this.email,
      titulo: this.formulario.value.titulo,
      imagem: this.imagem[0]
    });

    const acompanhamentoUpload = interval(1500);

    const continua = new Subject();
    continua.next(true);

    acompanhamentoUpload
      .pipe(takeUntil(continua))
      .subscribe(() => {
        /* console.log(this.progresso.status);
        console.log(this.progresso.estado); */

        this.progressoPublicacao = 'andamento';
        this.porcentagemUpload = Math.round((this.progresso.estado.bytesTransferred / this.progresso.estado.totalBytes) * 100);
        this.progress = { width: `${this.porcentagemUpload}%` };
        if (this.progresso.status === 'Concluído') {

          this.progressoPublicacao = 'concluido';

          // emitir evento para componente pai
          this.atualizarTimeLine.emit();

          continua.next(false);
        }
      });
  }

  public preparaImagemUpload(event: Event) {
    this.imagem = (<HTMLInputElement>event.target).files;
  }

}
