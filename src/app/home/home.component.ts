import { Component, OnInit, ViewChild } from '@angular/core';
import { AutentificacaoService } from '../autentificacao.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild('publicacoes') public publicacoes: any;

  constructor(
    private autentificacao: AutentificacaoService
  ) { }

  ngOnInit() {
  }

  public sair(): void {
    this.autentificacao.sair();
  }

  public atualizarTimeLine(): void {
    this.publicacoes.atualizarTimeLine();
  }
}
