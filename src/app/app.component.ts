import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit(): void {
    const config = {
      apiKey: 'AIzaSyCWI06O2PUjzDykFo2rYkXsYFm3mj2WQ3E',
      authDomain: 'jns-instagram-clone.firebaseapp.com',
      databaseURL: 'https://jns-instagram-clone.firebaseio.com',
      projectId: 'jns-instagram-clone',
      storageBucket: 'jns-instagram-clone.appspot.com',
      messagingSenderId: '357733634546'
    };
    firebase.initializeApp(config);
  }
}
