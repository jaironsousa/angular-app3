import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from './acesso/usuario.model';
import * as firebase from 'firebase';
@Injectable({
  providedIn: 'root'
})
export class AutentificacaoService {

  public token_id: string;

  constructor(
    private router: Router,
  ) { }

  public cadastrarusuario(usuario: Usuario): Promise<any> {
    return firebase.auth().createUserWithEmailAndPassword(usuario.email, usuario.senha)
      .then((resposta: any) => {

        // remover o atributo senha
        delete usuario.senha;
        // registrando dados do usario complementares do usuario no path email na base64
        firebase.database().ref(`usuario_detalhe/${btoa(usuario.email)}`)
          .set(usuario);
      })
      .catch((error: Error) => {
        console.log(error);
        alert(error.message);
      });
  }

  public autenticar(email: string, senha: string): any {
    firebase.auth().signInWithEmailAndPassword(email, senha)
      .then((resposta: any) => {
        console.log(resposta);
        firebase.auth().currentUser.getIdToken()
          .then((idToken: string) => {
            this.token_id = idToken;
            localStorage.setItem('idToken', idToken);
            this.router.navigate(['/home']);
          });
      })
      .catch((error: Error) => {
        console.log(error.message);
      });
  }

  public autenticado(): boolean {
    if (!this.token_id && !!localStorage.getItem('idToken')) {
      this.token_id = localStorage.getItem('idToken');
    }
    if (!this.token_id) {
      this.router.navigate(['/']);
    }
    return !!this.token_id;
  }

  public sair() {
    firebase.auth().signOut()
      .then(() => {
        localStorage.removeItem('idToken');
        this.token_id = undefined;
      });
  }

}
