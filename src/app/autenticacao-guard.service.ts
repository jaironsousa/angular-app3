import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { AutentificacaoService } from './autentificacao.service';

@Injectable()
export class AutenticacaoGuard implements CanActivate {

    constructor(
        private autenticacao: AutentificacaoService
    ) { }

    canActivate(): boolean {
        return this.autenticacao.autenticado();
    }
}
