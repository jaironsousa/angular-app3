import { Publicacao } from './home/publicacoes/publicacao.model';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Progresso } from './progresso.service';
import { resolve } from '../../node_modules/@types/q';
@Injectable({
  providedIn: 'root'
})
export class BdService {

  constructor(
    private progresso: Progresso,
  ) { }

  public publicar(publicacao: any): void {
    firebase.database().ref(`publicacoes/${btoa(publicacao.email)}`)
      .push({ titulo: publicacao.titulo })
      .then((resposta: any) => {
        const nomeImagem = resposta.key;
        firebase.storage().ref()
          .child(`imagens/${nomeImagem}`)
          .put(publicacao.imagem)
          .on(firebase.storage.TaskEvent.STATE_CHANGED,
            // acompanhamento do progressp do Upload
            (snapshot: any) => {
              this.progresso.status = 'Andamento';
              this.progresso.estado = snapshot;
              // console.log('Snapshot capturado on', snapshot);
            },
            (error) => {
              this.progresso.status = 'erro';
              console.log(error);
            },
            () => {
              // finalização do progresso
              this.progresso.status = 'Concluído';
              /* console.log('Upload completo'); */
            }
          );
      });
  }

  public consultarPublicacoes(emailUsuario: string): Promise<any> {

    // tslint:disable-next-line:no-shadowed-variable
    return new Promise((resolve, reject) => {
      // consultar as publicações no firebase
      firebase.database().ref(`publicacoes/${btoa(emailUsuario)}`)
        .orderByKey()
        .once('value')
        .then((snapshot: any) => {
          console.log(snapshot.val());

          let publicacoes: Array<any> = [];
          /* console.log(snapshot.val()); */
          snapshot.forEach((childSnapshot) => {
            let publicacao = childSnapshot.val();
            publicacao.key = childSnapshot.key;
            console.log(publicacao);

            publicacoes.push(publicacao);

          });

          console.log(publicacoes);
          return publicacoes.reverse();
        })
        .then((publicacoes: any) => {

          publicacoes.forEach(publicacao => {
            // consultar a url da imagem
            firebase.storage().ref()
              .child(`imagens/${publicacao.key}`)
              .getDownloadURL()
              .then((url: string) => {
                publicacao.url_imagem = (url);

                // consultar o nome do usuario
                firebase.database().ref(`usuario_detalhe/${btoa(emailUsuario)}`)
                  .once('value')
                  .then((snapshop: any) => {
                    publicacao.nome_usuario = snapshop.val().nome_usuario;
                  });
              });
            publicacoes.push(publicacao);
          });

          resolve(publicacoes);
        });
    });

  }

}
