import { AutentificacaoService } from './../../autentificacao.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter();

  public formulario: FormGroup = new FormGroup({
    'email': new FormControl(null),
    'senha': new FormControl(null),
  });

  constructor(
    private autentificacao: AutentificacaoService
  ) { }

  ngOnInit() {
  }

  public exibirPainelCadastro() {
    this.exibirPainel.emit('cadastro');
  }

  public autenticar() {
    this.autentificacao.autenticar(
      this.formulario.value.email,
      this.formulario.value.senha
    );
  }
}
